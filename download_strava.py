# jdluhos
import sys
import os
import re
import requests
from selenium import webdriver


def cls():
    os.system('cls' if os.name == 'nt' else 'clear')


def makeConnection():
    # Make connection socket to Strava
    browser.get(baseURL)
    # fillup email and password field
    mail = browser.find_element_by_name('email')
    pswd = browser.find_element_by_name('password')
    submit = browser.find_element_by_xpath("//button[contains(text(),'Log In')]")
    mail.send_keys(userStr)
    pswd.send_keys(passStr)
    submit.click()


def isConnected():
    # Check if we succeed with connection
    if browser.current_url == "https://www.strava.com/dashboard":
        return 1
    else:
        return 0


def foldersInit(uid):
    # Folders initialization
    if not os.path.exists(getUIDFolder(uid)):
        os.makedirs(getUIDFolder(uid))
    if not os.path.exists(getStravaFolder(uid)):
        os.makedirs(getStravaFolder(uid))


def getUIDFolder(uid):
    uidFolder = "toImport/" + uid
    return uidFolder


def getStravaFolder(uid):
    stravaFolder = getUIDFolder(uid) + "/strava/"
    return stravaFolder


reload(sys)

# get login information
cls()
if len(sys.argv) > 1:
    uid = sys.argv[1]
    userStr = sys.argv[2]
    passStr = sys.argv[3]
else:
    userStr = raw_input("Email: ")
    passStr = raw_input("Heslo: ")
    uid = raw_input("UID: ")

# Initialize folders
foldersInit(uid)

# go to strava
baseURL = 'https://www.strava.com/login'
browser = webdriver.PhantomJS('./phantomjs.exe' if os.name == 'nt' else './phantomjs for linux TBD')
makeConnection()

if isConnected():
    # print "You are logged in"
    browser.get("https://www.strava.com/athlete/training")
    if browser.current_url == "https://www.strava.com/athlete/training":
        # print "a Jsi v training sekci"

        links = browser.find_elements_by_xpath("//a[@href]")
        regex = "\/activities\/\d{1,10}$"
        counter = 0

        for link in links:
            if bool(re.search(regex, link.get_attribute("href"))):
                # we have a workout link and we have to let php script know to do not process yet
                with open(getStravaFolder(uid) + "status", "w") as status_file:
                    status_file.write("0")
                counter = + 1

                # now hack to download workouts via requests with help of cookies
                download_link = link.get_attribute("href") + '/export_gpx'
                session = requests.Session()
                cookies = browser.get_cookies()

                for cookie in cookies:
                    session.cookies.set(cookie['name'], cookie['value'])
                response = session.get(download_link)

                # saving response to gpx file
                filename = str(counter) + ".gpx"
                with open(getStravaFolder(uid) + filename, "w") as workout_file:
                    workout_file.write(str(response.content))

                print "workout number " + str(counter) + " has been saved to file " + str(filename) + "\n"

        # now php script has green flag to proccess what is in folder
        with open(getStravaFolder(uid) + "status", "w") as status_file:
            status_file.write("1")
else:
    # print "You are (NOT) logged in"
    exit(1)
