# jdluhos
import sys
import os
import re
import requests
from selenium import webdriver
from time import sleep


def cls():
    os.system('cls' if os.name == 'nt' else 'clear')


def makeConnection():
    # Make connection socket to Endomondo
    browser.get(baseURL)
    link = browser.find_element_by_class_name('header-guest-login-link')
    link.click()

    # fillup email and password field
    sleep(3)
    mail = browser.find_element_by_name('email')
    pswd = browser.find_element_by_name('password')
    submit = browser.find_element_by_xpath("//button[contains(text(),'Log In')]")
    mail.send_keys(userStr)
    pswd.send_keys(passStr)
    submit.click()
    sleep(3)


def isConnected():
    # Check if we succeed with connection
    if browser.current_url == "https://www.endomondo.com/home":
        return 1
    else:
        return 0


def foldersInit(uid):
    # Folders initialization
    if not os.path.exists(getUIDFolder(uid)):
        os.makedirs(getUIDFolder(uid))
    if not os.path.exists(getEndomondoFolder(uid)):
        os.makedirs(getEndomondoFolder(uid))


def getUIDFolder(uid):
    uidFolder = "toImport/" + uid
    return uidFolder


def getEndomondoFolder(uid):
    endomondoFolder = getUIDFolder(uid) + "/endomondo/"
    return endomondoFolder


reload(sys)

# get login information
cls()
if len(sys.argv) > 1:
    uid = sys.argv[1]
    userStr = sys.argv[2]
    passStr = sys.argv[3]
else:
    userStr = raw_input("Email: ")
    passStr = raw_input("Heslo: ")
    uid = raw_input("UID: ")

# Initialize folders
foldersInit(uid)

# Go to endomondo
baseURL = 'https://www.endomondo.com/?language=EN'
browser = webdriver.PhantomJS('./phantomjs.exe' if os.name == 'nt' else './phantomjs for linux TBD')
makeConnection()

if isConnected():
    print "You are logged in"
else:
    print "You are (NOT) logged in"
