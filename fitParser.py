from fitparse import FitFile

gpxHeaderXML = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\" ?>"
gpxHeaderGPX = "<gpx version=\"1.1\" creator=\"Toilster.com fit parser\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd http://www.cluetrust.com/XML/GPXDATA/1/0 http://www.cluetrust.com/Schemas/gpxdata10.xsd http://www.garmin.com/xmlschemas/TrackPointExtension/v1 http://www.garmin.com/xmlschemas/TrackPointExtensionv1.xsd\" xmlns:gpxdata=\"http://www.cluetrust.com/XML/GPXDATA/1/0\" xmlns:gpxtpx=\"http://www.garmin.com/xmlschemas/TrackPointExtension/v1\" xmlns=\"http://www.topografix.com/GPX/1/1\">"
gpxStructureHeader = "  <trk>" \
                     "<name>Toilster fit parser</name>" \
                     "<trkseg>"

fitfile = FitFile('./sample-activity.fit')

gpxRecordBody = 'here we start '


def getFormattedRecord(record_data):
    output = record_data.get('postition_lat')

    return output
    # return 'latitude is: ' + record_data.value  # Get all data messages that are of type record
for record in fitfile.get_messages('record'):
    gpxRecordBody += str(getFormattedRecord(record))

    # Go through all the data entries in this record
    # for record_data in record:
    # Print the records name and value (and units if it has any)
    # if record_data.units:
    #    print " * %s: %s %s" % (
    #        record_data.name, record_data.value, record_data.units,
    #    )
    # else:
    #    print " * %s: %s" % (record_data.name, record_data.value)


    # if record_data.units:
    #    if(record_data.name == 'position_lat'):
    #        print "latitude is: " + str(record_data.value)
    # else:
    #    print " * %s: %s" % (record_data.name, record_data.value)

    # gpxRecordBody += getFormattedRecord(record_data)

    print str(gpxRecordBody)
